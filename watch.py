from re import I
import PySimpleGUI as gui
from time import time


program_name = "Stop watch"
start_time = 0
active = False
lap_amount = 1


def create_window():
    gui.theme('Black')
    layout = [[gui.VPush()],
              [gui.Text('', expand_x=True, justification='center',
                        font='Ariel 50', key='-TIME-')],
              [gui.Button('Start', expand_x=True, key='-START-'),
               gui.Button('Lap', expand_x=True, key='-LAP-', visible=False
                          )],
              [gui.Column([[]], key='-LAPS-')],
              [gui.VPush()]]
    return gui.Window(program_name, layout, size=(300, 300))


window = create_window()

while True:
    event, Values = window.read(timeout=10)

    if event == gui.WINDOW_CLOSED:
        break

    if event == '-START-':
        if active:
            # active to stop
            active = False
            window['-START-'].update('Start')
            window['-LAP-'].update(visible=False)
            lap_amount = 1

        else:
            # from start to reset
            if start_time > 0:
                window.close()
                window = create_window()
                start_time = 0

            # start to active
            else:
                start_time = time()
                active = True
                window['-START-'].update('Stop')
                window['-LAP-'].update(visible=True)

    if event == '-LAP-':
        window.extend_layout(
            window['-LAPS-'], [[gui.Text(lap_amount), gui.VerticalSeparator(), gui.Text(round(time_passed,2))]])
        lap_amount += 1

    if active:
        time_passed = time() - start_time
        window['-TIME-'].update(round(time_passed, 1))

window.close()
